//
//  ViewController.swift
//  ScheduleTask
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import SVProgressHUD
class ViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    @IBOutlet weak var lblday1: UILabel!
    @IBOutlet weak var lblDay2: UILabel!
    @IBOutlet weak var lblday3: UILabel!
    @IBOutlet weak var lblDay4: UILabel!
    @IBOutlet weak var lblDay5: UILabel!
    @IBOutlet weak var lblDay6: UILabel!
    @IBOutlet weak var lblDay7: UILabel!
    @IBOutlet weak var lblTopDate: UILabel!

    let formatter = DateFormatter()
    var result : String?
    var subjectArray : [String]?
    var txt : String = ""
    var userModel : Hitdata?
    var temp : String = ""
    let date = Date()
    var lastdate : String = ""
    var pageNumber : Int = 1
    var updateArray = [iData]()
    override func viewDidLoad() {
        super.viewDidLoad()

          sendData()

    }
 
 
    
// Day converter
    
    func getDayOfWeekString(today:String)->String? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let todayDate = formatter.date(from: today) {
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let myComponents = myCalendar.components(.weekday, from: todayDate)
            let weekDay = myComponents.weekday
            switch (weekDay!) {            
            case 1:
                return "Sun"
            case 2:
                return "Mon"
            case 3:
                return "Tues"
            case 4:
                return "Wed"
            case 5:
                return "Thu"
            case 6:
                return "Fri"
            case 7:
                return "Sat"
         default:
                return "Nil"
            }
        }
        return nil
    }
    
// Api Hit
    func sendData() {
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        let param:[String:Any] = ["access_token":"ymaANbhfJT4ARby5IbK2u0hUJQ9T7dk8", "page_no": pageNumber,"page_size":"7","date_selected": result,"last_date": lastdate]
        SVProgressHUD.show()
        ApiHit.fetchData(urlStr: "instructor-home", parameters: param) { (jsonData) in
             print("\(jsonData)")
            self.userModel = Mapper<Hitdata>().map(JSONObject: jsonData)
            for index in 0...6
            {
                self.updateArray.append((self.userModel?.idata?[index])!)
            }
            self.collectionView.reloadData()
            SVProgressHUD.dismiss()
            self.lblday1.text = self.getDayOfWeekString(today:(self.userModel?.idata?[0].date1!)!)
            self.lblDay2.text = self.getDayOfWeekString(today:(self.userModel?.idata?[1].date1!)!)
            self.lblday3.text = self.getDayOfWeekString(today:(self.userModel?.idata?[2].date1!)!)
            self.lblDay4.text = self.getDayOfWeekString(today:(self.userModel?.idata?[3].date1!)!)
            self.lblDay5.text = self.getDayOfWeekString(today:(self.userModel?.idata?[4].date1!)!)
            self.lblDay6.text = self.getDayOfWeekString(today:(self.userModel?.idata?[5].date1!)!)
            self.lblDay7.text = self.getDayOfWeekString(today:(self.userModel?.idata?[6].date1!)!)
            self.lblTopDate.text = ("\(self.userModel?.idata?[0].date ?? "")")
            print(self.userModel?.msg ?? "")
        }
    }
}

extension ViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
   //   return (self.userModel?.idata?.count) ?? 0
        return (self.updateArray.count)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell:CollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "identifierCollectionCell", for: indexPath) as? CollectionViewCell) else{return CollectionViewCell()}
    //    cell.detailsArray = (self.userModel?.idata?[indexPath.row].details)!
        cell.detailsArray = updateArray[indexPath.item].details
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let myWidth = collectionView.frame.width
        let myHeight = collectionView.frame.height
        return CGSize(width: myWidth, height:myHeight)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        var visibleRect = CGRect()
        visibleRect.origin = collectionView.contentOffset
        visibleRect.size = collectionView.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath: IndexPath = collectionView.indexPathForItem(at: visiblePoint)!

        self.lblTopDate.text = ("\(self.updateArray[visibleIndexPath.item].date ?? "")")

        if(visibleIndexPath.item == (updateArray.count - 1))
        {
            pageNumber=pageNumber+1
            //lastDate=(detailDataMain?.data?[(detailDataMain?.data?.count)! - 1].date)!
            lastdate=(self.updateArray[updateArray.count - 1].date1)!
            self.sendData()
        }

    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath){
        switch (indexPath.item % 7) {
        case 0:
            highLight(label: lblday1)
        case 1:
            highLight(label: lblDay2)
        case 2:
            highLight(label: lblday3)
        case 3:
            highLight(label: lblDay4)
        case 4:
            highLight(label: lblDay5)
        case 5:
            highLight(label: lblDay6)
        case 6:
            highLight(label: lblDay7)
        default:
            print("Out of index")
        }
        
    }
    func highLight(label:UILabel){
        var Array = [ lblday1, lblDay2, lblday3, lblDay4, lblDay5, lblDay6, lblDay7]
        for index in 0..<Array.count
        {
            if(label == Array[index]){
                label.font=UIFont.boldSystemFont(ofSize: 20)
            }
            else{
                Array[index]?.font=UIFont.boldSystemFont(ofSize: 15)
            }
        }
    }

}
