//
//  CollectionViewCell.swift
//  ScheduleTask
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    var detailsArray : [Details]?
     //   var
    
    @IBOutlet weak var tableView: UITableView!

override func awakeFromNib() {
    super.awakeFromNib()
    tableView.delegate = self
    tableView.dataSource = self
}
}

extension CollectionViewCell: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    return detailsArray!.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        guard let cell:TableViewCell = (tableView.dequeueReusableCell(withIdentifier: "identifierTableCell", for: indexPath) as? TableViewCell) else{return TableViewCell()}
                cell.lblSubject.text = self.detailsArray?[indexPath.row].subjects?[0].subject_name
                cell.lblEnrolled.text = "\(self.detailsArray?[indexPath.row].enrolled ?? 0)" + "/" + "\(self.detailsArray?[indexPath.row].max_student ?? 0)"
                cell.lblAddress.text = self.detailsArray?[indexPath.row].day_location
                cell.lblAge.text = self.detailsArray?[indexPath.row].age_group
                cell.lblFee.text = self.detailsArray?[indexPath.row].charge_student
                cell.lblTime.text = self.detailsArray?[indexPath.row].time_duration
                cell.lblStartTime.text = self.detailsArray?[indexPath.row].start_time1
                if(indexPath.row == 0 ){
                cell.lineViewUpper.backgroundColor = UIColor.white
                }
                else {
                    cell.lineViewUpper.backgroundColor = UIColor(red: 81/255,green: 251/255 , blue: 253/255, alpha: 1)
                }
                if(indexPath.row == detailsArray!.count - 1)
                {
                cell.lineViewLower.backgroundColor = UIColor.white
                }
                else{
                    cell.lineViewLower.backgroundColor = UIColor(red: 81/255,green: 251/255 , blue: 253/255, alpha: 1)
                }
        return cell
    }
    
}


