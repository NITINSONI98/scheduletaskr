//
//  TableViewCell.swift
//  ScheduleTask
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var viewDot: UIImageView!
    
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblEnrolled: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblFee: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var lineViewUpper: UIView!
    
    @IBOutlet weak var lineViewLower: UIView!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 2
        viewDot.layer.cornerRadius = 5
    }
}
